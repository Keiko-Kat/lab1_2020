#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
#include "Persona.h"
using namespace std;

//Se crea la clase ejercicio3 que contiene los datos del programa//
class Ejercicio3{
	//Se define la variable privada y se inicializa como 0 para correr el programa// 
	private: 
		int numero = 0;
		
	public:
	//Se define un constructor vacio para crear el objeto en la funcion main//
		Ejercicio3(){}
		
		
		//catch_string se asegura de evitar un error de conversion//
		//Asegurando que solo existan numeros en el string ingresado//
		string catch_string(string in){
			//Esta funcion recibe un string ingresado por teclado en la funcion principal//
			//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9 y al valor de '-'//
					if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
						//si el contenido del string no coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//De esta forma se asegura que la funcion 'stoi()' no entregue error//
			return in;
		}
		
		//Catch_int es una funcion que evita el ingreso de valores numericos a los ingresos de string//
		//Recibe un string que es ingresado por teclado//
		string catch_int(string in){
		//Para validar la entrada de un int primero se define la variable booleana is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9//
					if(in[j] < 48 || in[j] >57){
						//Si el contenido del string coincide con un valor ASCII numerico is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra un numero en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor no ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//Esta funciona segura la ausencia de numeros en el string ingresado//
			return in;
		}
		
		//La funcion mors recibe un string entregado pro teclado y regresa un booleano//
		bool mors(string in){
			//primero se crean dos variables bool, una para correr el programa y otra para retornar el valor a "run()"//
			bool is_true = true, moroso;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre una condicional if que analiza el string recibido//
				if (in == "S" || in == "N"){
					//Si el contenido del string corresponde a una de las opciones validas//
					//Se llena la variable moroso con el valor que corresponda//
					if(in == "S"){
						moroso = true;
					}else{
						moroso = false;
					}
					//Y la variable is_true se transforma a false//
					is_true = false;
				}
				//Si el contenido no corresponde a una opcion viable//
				if(is_true == true){
					//Se le pide al usuario que ingrese nuevamente una opcion//
					cout<<"Por favor ingrese una opcion valida: (S/N)"<<endl;
					//Se recibe por teclado//
					getline(cin, in);
				}
				//Y se reinicia el ciclo//
			}
			//De is_true resultar falso, se retorna el valor ya guardado dentro de moroso//
			return moroso;
		}
		
		void llena(Persona *arr[]){
			//Se crean tres variables string, una para recibir el ingreso del teclado y dos para guardar copias temporales de los ingresos de teclado//
			string in, nombre, telefono;
			//Un int para guardar un ingreso//
			int saldo;
			//Y un bool para guardar temporalmente el estado del cliente//
			bool moroso;
			cout<<endl;
			//Se abre un for que recorre el arreglo completo//	
			for(int i = 0; i < this->numero; i++){
				//Se le pide al usuario el nombre del cliente//
				cout<<"Ingrese el nombre del cliente: "<<endl;
				//Se recibe como ingreso de teclado//
				getline(cin, in);
				//Y se comprueba que no se ingresen valores numericos//
				in = catch_int(in);
				//Para luego guardarlo en una variable string//	
				nombre = in;
				//Se le pide al usuario el telefono del cliente//
				cout<<"Ingrese el telefono del cliente: "<<endl;
				//Se recibe como ingreso de teclado//
				getline(cin, in);
				//Y se comprueba que sea un valor completamene numerico//
				in = catch_string(in);
				//Para luego guardarlo en una variable string//	
				telefono = in;
				//Se le pide al usuario el saldo del cliente//
				cout<<"Ingrese el saldo del cliente: "<<endl;
				//Se recibe como ingreso de teclado//
				getline(cin, in);
				//Y se comprueba que sea un valor completamene numerico//
				in = catch_string(in);
				//Para luego transformarlo a una variable int//	
				saldo = stoi(in);
				//Y se le píde al usuario el estado del cliente//
				cout<<"Es el cliente moroso? (S/N)"<<endl;
				//Se recibe como ingreso de teclado//
				getline(cin, in);	
				//Y la variable tipo bool se recibe de una funcion que lee el ingreso//
				moroso = mors(in);
				//Luego todas las variables ingresadas se utilizan en el contructor de persona//
				//Para crear los contenidos del array//
				arr[i] = new Persona(nombre, telefono, saldo, moroso);
				cout<<endl;
			}
		}
		
		//La funcion printer recibe el array ya lleno y muestra por pantalla su contenido//
		void printer(Persona *arr[]){
			//Primero se crea un string para guardar la salida apropiada de la caracteristica moroso//
			string mors;			
			//Se abre un for que recorre el arreglo completo//	
			for(int i = 0; i < this->numero; i++){
				//Y lo primero que se hace es definir la salida dependiendo de el valor de la variable bool moroso//
				if(arr[i]->get_moroso() == true){
					mors = "Moroso";
				}else{
					mors = "No moroso";
				} 
				//Para luego imprimir por pantalla los contenidos de cada objeto persona dentro del array//
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"||"<<i+1<<")"<<endl;
				cout<<"|| Nombre Cliente:    |> "<< arr[i]->get_nombre()<<endl;
				cout<<"|| Telefono Cliente:  |> "<< arr[i]->get_telefono()<<endl;
				cout<<"|| Saldo Cliente:     |> "<< arr[i]->get_saldo()<<endl;
				cout<<"|| Estado Cliente:    |> "<< mors<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
			}			
		}
		
		void run(){
			//Se crea un string para recibir las entradas de teclado//
			string in;
			//Se le pide al usuario que ingrese el tamaño del arreglo//
			cout<<"Ingrese el tamaño del arreglo a llenar: ";
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Se utiliza una funcion para evitar que se ingresen valores numericos//
			in = catch_string(in);
			//Y se guarda el ingreso en la variable de clase numero//
			this->numero = stoi(in);
			//Se crea un arreglo vacio de tamaño numero//
			Persona *arr[this->numero];
			//Se llama una funcion que llena el arreglo//
			llena(arr);
			//Y finalmente se llama a una funcion que imprime los contenidos del arreglo//
			printer(arr);			
		}		
};
