#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
#include "Persona.h"
#include "Programa3.h"

int main(){
	//Se crea el objeto tipo Ejercicio3 para correr el programa//
	Ejercicio3 *pro = new Ejercicio3();
	//Se llama a la funcion run de Ejercicio3//
	//run() contiene las instrucciones necesarias para funcionar//
	pro->run();	
	//Se retorna 0 para acabar el main//

	return 0;
}
