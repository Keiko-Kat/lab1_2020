#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
using namespace std;

#ifndef _PERSONA_H_
#define _PERSONA_H_

class Persona{
	//Primero se define la clase Persona//
	private:
	//Luego se definen las caracteristicas privadas de Persona//
	//Las que se inicializan con valores neutros//
		string nombre = "\0";
		string telefono = "\0";
		int saldo = 0;
		bool moroso = false;
	public:
	//se crean dos constructores//
	//Uno por defecto y uno que recibe cuatro parametros// 
		Persona();
		Persona(string nombre, string telefono, int saldo, bool moroso);
			
	//Se definen inicialmente metodos set, que reciben parametros ingresados por el usuario//
		void set_nombre(string nombre);
		void set_telefono(string telefono);
		void set_saldo(int saldo);
		void set_moroso(bool moroso);
		
	//Y se definen metodos get, que retornan los parametros pertenecientes al objeto tipo Persona//
		string get_nombre();
		string get_telefono();
		int get_saldo();
		bool get_moroso();
};

#endif /*_PERSONA_H_*/
