#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
#include "Persona.h"
using namespace std;

Persona::Persona(){}

//El contructor Persona recibe cuatro parametros entregados por el usuario//
Persona::Persona(string nombre, string telefono, int saldo, bool moroso){
	//Y estos parametros se insertan en las variables correspondientes//
	this->nombre = nombre;
	this->telefono = telefono;
	this->saldo = saldo;
	this->moroso = moroso;
}

//El metodo set de nombre, recibe un string entregado por el usuario//
void Persona::set_nombre(string nombre){
	//El cual se inserta en la caracteristica nombre del objeto Persona//
	this->nombre = nombre;
}
//El metodo set de telefono, recibe un string entregado por el usuario//
void Persona::set_telefono(string telefono){
	//El cual se inserta en la caracteristica telefono del objeto Persona//
	this->telefono = telefono;
} 
//El metodo set de saldo recibe un int entregado por el usuario//
void Persona::set_saldo(int saldo){
	//El cual se inserta en la caracteristica saldo del objeto Persona//
	this->saldo = saldo;
}
//El metodo set de saldo recibe un bool entregado por el usuario//
void Persona::set_moroso(bool moroso){
	//El cual se inserta en la caracteristica moroso del objeto Persona//
	this->moroso = moroso;
}

//Los metodos get, extraen la informacion almacenada en el objeto Persona//
//Para que el usuario pueda accesarlos//	
string Persona::get_nombre(){
	return this->nombre;
}
string Persona::get_telefono(){
	return this->telefono;
}
int Persona::get_saldo(){
	return this->saldo;
}
bool Persona::get_moroso(){
	return this->saldo;
}
