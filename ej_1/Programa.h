#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
using namespace std;
#ifndef _EJERCICIO_H_
#define _EJERCICIO_H_
//Se crea la clase ejercicio que contiene los datos del programa//
class Ejercicio{
	//Se definen las variables privadas y se inicializan como 0 para correr el programa//  
	private: 
		int numero = 0;
		int suma = 0;
		
	public:
	//Se define un constructor vacio para crear el objeto en la funcion main// 
		Ejercicio(){}
		
		
		//catch_string se asegura de evitar un error de conversion//
		//Asegurando que solo existan numeros en el string ingresado//
		string catch_string(string in){
			//Esta funcion recibe un string ingresado por teclado en la funcion principal//
			//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9 y al valor de '-'//
					if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
						//si el contenido del string coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//De esta forma se asegura que la funcion 'stoi()' no entregue error//
			return in;
		}
		
		void llena(int arr[]){
			//Esta funcion recibe un array vacio para llenar//
			//Se crea un string para recibir las entradas de teclado//
			string in;
			//Y se crea un int para recibir un resultado numerico//
			int temp;
			//Se abre un for que recorre el arreglo completo//
			for(int i = 0; i < this->numero; i++){
				//Se le pide al usuario que ingrese una cantidad numerica//
				cout<<"Ingrese numero"<<endl;
				//Se recibe como ingreso de teclado//
				getline(cin, in);
				//Y se comprueba que sea un valor completamene numerico//
				in = catch_string(in);
				//Para luego transformarlo a una variable int//		
				temp = stoi(in);
				//Y la variable int se guarda en el arreglo//
				arr[i] = temp;
			}
		}
				
		int cuadrados(int arr[]){
			//Esta funcion recibe el array de ints lleno//
			//Se abre un for que recorre el arreglo completo//
			for(int i = 0; i < this->numero; i++){
				//Se suman los cuadrados de los contenidos y se guardan en suma//
				this->suma += (arr[i] * arr[i]);	
			}
			//Suma se retorna a la funcion principal//
			return this->suma;
		}
		
		void run(){
			//Se crea un string para recibir las entradas de teclado//
			string in;
			//Y se crea un int para recibir un resultado numerico//
			int salida;
			//Se le pide al usuario que ingrese el tamaño del arreglo//
			cout<<"Ingrese el tamaño del arreglo a llenar: ";
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Se utiliza una funcion para evitar que se ingresen valores numericos//
			in = catch_string(in);
			//Y se guarda el ingreso en la variable de clase numero//
			this->numero = stoi(in);
			//Se crera un arreglo vacio de tamaño numero//
			int arr[this->numero];
			//Se llama una funcion que llena el arreglo//
			llena(arr);
			//Se llena salida con una funcion que retorna lo pedido//
			salida = cuadrados(arr);
			//Y finalmente se muestra por pantalla el resultado//
			cout<<"La suma de los cuadrados de los numeros ingresados es: "<<salida<<endl;
		}
};
#endif /* _EJERCICIO_H_ */
				
				
				
