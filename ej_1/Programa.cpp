#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
#include "Programa.h"

int main(){
	//Se crea el objeto tipo Ejercicio para correr el programa//
	Ejercicio *pro = new Ejercicio();
	//Se llama a la funcion run de Ejercicio//
	//run() contiene las instrucciones necesarias para funcionar//
	pro->run();	
	//Se retorna 0 para acabar el main//
	return 0;
}
