#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
#include <cctype>
using namespace std;
#ifndef _EJERCICIO2_H_
#define _EJERCICIO2_H_
//Se crea la clase ejercicio2 que contiene los datos del programa//
class Ejercicio2{
	//Se define la variable privada y se inicializa como 0 para correr el programa// 
	private: 
		int numero = 0;
		
	public:
	//Se define un constructor vacio para crear el objeto en la funcion main// 
		Ejercicio2(){}
		
		
		void llena(string arr[]){
			//Esta funcion recibe un array vacio para llenar//
			//Se crea un string para recibir las entradas de teclado//
			string in;
			//Se abre un for que recorre el arreglo completo//
			for(int i = 0; i < this->numero; i++){
				//Se le pide al usuario que ingrese una frase//
				cout<<"Ingrese frase por favor"<<endl;
				//Se recibe como ingreso de teclado//
				getline(cin, in);
				//Y se comprueba que sea un valor completamene numerico//
				in = catch_int(in);
				//Para luego guardarlo en el arreglo//					
				arr[i] = in;
			}
		}
		
		
		//Catch_int es una funcion que evita el ingreso de valores numericos a los ingresos de string//
	//Recibe un string que es ingresado por teclado//
		string catch_int(string in){
	//Para validar la entrada de un int primero se define la variable booleana is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9//
					if(in[j] < 48 || in[j] >57){
						//Si el contenido del string coincide con un valor ASCII numerico is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra un numero en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor no ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//Esta funciona segura la ausencia de numeros en el string ingresado//
			return in;
		}
		
		//catch_string se asegura de evitar un error de conversion//
		//Asegurando que solo existan numeros en el string ingresado//
		string catch_string(string in){
			//Esta funcion recibe un string ingresado por teclado en la funcion principal//
			//para validar la entrada de un int primero se crea y define is_true como verdadero por default//
			bool is_true = true;
			//Se abre un ciclo while que corre mientras is_true sea verdadero//
			while (is_true == true){
				//Dentro del while se abre un ciclo for que analiza el string recibido//
				for(int j = 0; j < in.size(); j++){
					//Se recorre y se comparan los valores ASCII contenidos en el string//
					//A los valores ASCII de los numros del 0-9 y al valor de '-'//
					if((in[j] >= 48 && in[j] <= 57) || in[j] == 45){
						//si el contenido del string no coincide con un valor ASCII numerico o de "-" is_true se iguala a falso//
						is_true = false;
					}else{
						//Si el contenido del string si coincide, is_true se iguala a verdadero y se rompre el ciclo//
						is_true = true;
						break;
					}
				}
				//Luego si se encuentra algo que no es un numero o '-' en el string ingresado//
				if(is_true == true){
					//Se entrega un mensaje de error//
					cout<<"Por favor ingrese valores numéricos:"<<endl;
					//Y se pide otro ingreso//
					getline(cin, in);
				}
			}
			//De esta forma se asegura que la funcion 'stoi()' no entregue error//
			return in;
		}
		
		//La funcion results recibe el array de strings lleno// 
		void results(string arr[]){
			//Se crean dos variables int para contar las mayusculas y minusculas//
			int upp = 0, low = 0;
			//Y una variable string para leer los contenidos del array//
			string test;
			//Se abre un for que recorre el arreglo completo//			
			for(int i = 0; i < this->numero; i++){
				//Se llena test con el contenido de arr//
				test = arr[i];
				//Se abre un for que recorre el string completo//
				for(int j = 0; j < test.size(); j++){
					//Se lee cada componente con las finciones islower() e isupper()//
					//Y se suma uno al contador correspondiente//
					if(islower(test[j]) != 0){
						low++;
					}else{
						if(isupper(test[j]) != 0){
							upp++;
						}
					}
				}
				//Para finalmente imprimir por pantalla el resultado obtenido//
				cout<<endl;
				cout<<"El ingreso '"<<test<<"' posee "<<test.size()<<" caracteres;"<<endl;
				cout<<"De los cuales "<<low<<" caracteres estan en minuscula, y "<<upp<<" estan en mayuscula."<<endl;
				//Antes de acabar el ciclo los contadores se reinician a 0//
				upp = 0;
				low = 0;
			}		
		}
		

		
		void run(){
			//Se crea un string para recibir las entradas de teclado//
			string in;
			//Se le pide al usuario que ingrese el tamaño del arreglo//
			cout<<"Ingrese el tamaño del arreglo a llenar: ";
			//Se recibe como ingreso de teclado//
			getline(cin, in);
			//Se utiliza una funcion para evitar que se ingresen valores numericos//
			in = catch_string(in);
			//Y se guarda el ingreso en la variable de clase numero//
			this->numero = stoi(in);
			//Se crera un arreglo vacio de tamaño numero//
			string arr[this->numero];
			//Se llama una funcion que llena el arreglo//
			llena(arr);
			//Y finalmente se llama una funcion que calcula lo pedido y muestra los resultados por pantalla//
			results(arr);
			
		}
};
#endif /* _EJERCICIO2_H_ */
				
