#include <stdlib.h>
#include <iostream>
#include <string>
#include <list>
#include <math.h>
#include <cctype>
#include "Programa2.h"

int main(){
	
	//Se crea el objeto tipo Ejercicio2 para correr el programa//
	Ejercicio2 *pro = new Ejercicio2();
	//Se llama a la funcion run de Ejercicio2//
	//run() contiene las instrucciones necesarias para funcionar//
	pro->run();	
	//Se retorna 0 para acabar el main//
}
